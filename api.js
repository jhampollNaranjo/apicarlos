let url = 'https://swapi.co/api/films/'; 

//Creamos un objeto de tipo XMLHttpRequest para manejar nuestras peticiones
var xmlhttp = new XMLHttpRequest();

xmlhttp.open('GET', url, true);

xmlhttp.send();
xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var myObject = JSON.parse(this.responseText);
        console.log(myObject.results);
        let arr=myObject.results;
        for (let i = 0; i < arr.length; i++) {
            for (let j = 0; j < arr.length; j++) {
                var  contenido =`
                <div class="card col-md-4 pelicula">
                    <div class="card-body">
                        <h4 class="card-title">${arr[i].title}</h4>
                        <p class="card-text"><strong>Description</strong> : ${arr[i].opening_crawl}.</p>
                        <a  class="card-link"><strong>Director</strong> : ${arr[i].director}</a><br>
                        <a  class="card-link"><strong>Producer</strong> : ${arr[i].producer}</a><br>
                        <a  class="card-link"><strong>Release date</strong> : ${arr[i].release_date}</a>
                    </div>
                </div>`;
            }   
            $(".row").append(contenido); 
        }
       
    }
};

